package service;

import dto.AbstractRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public interface RequestService<R extends AbstractRequest> {

    default List<R> generateList(int size) {
        List<R> list = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            list.add(getById());
        }
        return list;
    }

    UUID create();

    R getById();

    default void deleteById() {

    }
}
