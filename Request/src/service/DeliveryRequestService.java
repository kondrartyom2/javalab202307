package service;

import dto.DeliveryRequest;
import dto.Poluchatel;
import dto.User;

import java.util.UUID;

public class DeliveryRequestService implements RequestService<DeliveryRequest> {
    @Override
    public UUID create() {
        return UUID.randomUUID();
    }

    @Override
    public DeliveryRequest getById() {
        return new DeliveryRequest(
                new User(UUID.randomUUID(), "Artem"),
                "Kazan",
                new Poluchatel(UUID.randomUUID(), "Artem", "9999 000000")
        );
    }
}
