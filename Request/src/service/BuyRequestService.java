package service;

import dto.BuyRequest;
import dto.User;

import java.util.UUID;

public class BuyRequestService implements RequestService<BuyRequest> {
    @Override
    public UUID create() {
        return UUID.randomUUID();
    }

    @Override
    public BuyRequest getById() {
        return new BuyRequest(
                new User(UUID.randomUUID(), "Artem"),
                10000
        );
    }
}
