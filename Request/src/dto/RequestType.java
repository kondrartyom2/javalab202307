package dto;
public enum RequestType {
    DELIVERY("Поставку"), SALE("Продажу"), BUY("Закупку");

    private final String description;

    RequestType(String description) {
        this.description = description;
    }
}
