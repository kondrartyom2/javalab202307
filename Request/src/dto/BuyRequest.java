package dto;

import java.util.UUID;

public class BuyRequest extends AbstractRequest {

    private Integer price;
    public BuyRequest(User user, Integer price) {
        super(UUID.randomUUID(), user, RequestType.BUY);
        this.price = price;
    }

    public Integer getPrice() {
        return price;
    }
}
