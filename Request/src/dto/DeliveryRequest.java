package dto;

import java.util.UUID;

public class DeliveryRequest extends AbstractRequest {
    private String address;
    private Poluchatel poluchatel;

    public DeliveryRequest(User user, String address, Poluchatel poluchatel) {
        super(UUID.randomUUID(), user, RequestType.DELIVERY);
        this.address = address;
        this.poluchatel = poluchatel;
    }

    public String getAddress() {
        return address;
    }

    public Poluchatel getPoluchatel() {
        return poluchatel;
    }
}
