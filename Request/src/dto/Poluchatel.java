package dto;

import java.util.UUID;

public class Poluchatel extends User {

    private String passport;

    public Poluchatel(UUID id, String name, String passport) {
        super(id, name);
        this.passport = passport;
    }
}
