package dto;

import java.util.Objects;
import java.util.UUID;

public abstract class AbstractRequest {

    private UUID id;
    private User user;
    private RequestType type;

    AbstractRequest(UUID id, User user, RequestType type) {
        this.id = id;
        this.user = user;
        this.type = type;
    }

    public UUID getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public RequestType getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractRequest request = (AbstractRequest) o;
        return Objects.equals(id, request.id) && Objects.equals(user, request.user) && type == request.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, type);
    }
}
